=== Run information ===

Scheme:weka.classifiers.lazy.IBk -K 10 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
Relation:     train
Instances:    212244
Attributes:   62
              weekInData
              productAndStoreID
              dailySales1
              dailySales2
              dailySales3
              dailySales4
              dailySales5
              dailySales6
              dailySales7
              dailySales8
              dailySales9
              dailySales10
              dailySales11
              dailySales12
              dailySales13
              dailySales14
              dailySales15
              dailySales16
              dailySales17
              dailySales18
              dailySales19
              dailySales20
              dailySales21
              dailySales22
              dailySales23
              dailySales24
              dailySales25
              dailySales26
              dailySales27
              dailySales28
              daysStoreClosed_series
              daysStoreClosed_target
              series_Pääsiäinen
              series_Vappu
              series_Äitienpäivä
              series_Juhannus
              series_Halloween
              series_Isänpäivä
              series_Itsenäisyyspäivä
              series_Joulu
              series_Uudenvuodenaatto
              series_Uudenvuodenpäivä
              series_Loppiainen
              series_Ystävänpäivä
              target_Pääsiäinen
              target_Vappu
              target_Äitienpäivä
              target_Juhannus
              target_Halloween
              target_Isänpäivä
              target_Itsenäisyyspäivä
              target_Joulu
              target_Uudenvuodenaatto
              target_Uudenvuodenpäivä
              target_Loppiainen
              target_Ystävänpäivä
              dayOfMonth
              dayOfYear
              weekOfYear
              month
              quarter
              target
Test mode:split 90.0% train, remainder test

=== Classifier model (full training set) ===

IB1 instance-based classifier
using 10 nearest neighbour(s) for classification


Time taken to build model: 0.19 seconds

=== Evaluation on test split ===
=== Summary ===

Correlation coefficient                  0.9811
Mean absolute error                      3.8305
Root mean squared error                 29.7918
Relative absolute error                 18.6244 %
Root relative squared error             23.1475 %
Total Number of Instances            21224     

