#!/bin/bash

report="$1"
submit="${2:-mm305678.txt}"

cat "$report" | sed 's/\s*[0-9]\+\s\+?\s\+\([0-9,.]\+\)\s\+.\+$/\1/;tx;d;:x' > "$submit"

[[ 9557 -eq $(wc -l "$submit" | cut -d" " -f 1) ]] || { echo "Failed..."; exit 1; }
