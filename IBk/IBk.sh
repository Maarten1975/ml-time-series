#!/bin/bash

class="weka.classifiers.lazy.IBk -K 5 -E -I -A \"weka.core.neighboursearch.KDTree -A \\\"weka.core.EuclideanDistance -R first-last\\\" -S weka.core.neighboursearch.kdtrees.MidPointOfWidestDimension\""

evaluate() {
  eval "java -classpath \"$CLASSPATH\" -Xmx3g $1 -no-cv -v -o -t ${2:-data/eval-train.arff} -T ${3:-data/eval-test.arff}"
}

classify() {
  eval "java -classpath \"$CLASSPATH\" -Xmx3g $1 -no-cv -v -o -t ${2:-data/train.arff} -T ${3:-data/test.arff} -p 0"
}

if [[ "$1" == -c* ]]; then
  classify "$class"
else
  evaluate "$class"
fi

