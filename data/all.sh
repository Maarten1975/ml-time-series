#!/bin/bash

if [[ $1 == "raw" ]]; then
  cat 'data/test.csv' <(tail -n +2 'data/train.csv')
else
  cat 'data/test.csv' <(tail -n +2 'data/train.csv') \
    | sed '2,$s/^\(.*\)_\([^_]*\)$/\1,\2/' \
    | sed '1s/^\(.*\),productAndStoreID,\(.*\)$/\1,productID,storeID,\2/'
fi

