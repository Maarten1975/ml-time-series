#include <fann.h>
#include <fann_cpp.h>

using namespace FANN;

const char model_file[] = "model.net";
const char data_file[] = "data/train.fann";

const unsigned int num_input = 2, num_output = 1, num_layers = 1,
      num_neurons_hidden = 3, max_epochs = 50, epochs_between_reports = 1000;
const float desired_error = (const float) 0.001;

int main() {
  /*
  neural_net net;
  net.create_standard(num_layers, num_input, num_neurons_hidden, num_output);
  net.set_activation_function_hidden(SIGMOID_SYMMETRIC);
  net.set_activation_function_output(SIGMOID_SYMMETRIC);
  net.train_on_file(data_file, max_epochs, epochs_between_reports,
      desired_error);
  net.save(model_file);
  */
  struct fann *ann = fann_create_standard(num_layers, num_input,
      num_neurons_hidden, num_output);
  fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
  fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC);
  fann_train_on_file(ann, data_file, max_epochs, epochs_between_reports,
      desired_error);
  fann_save(ann, model_file);
  fann_destroy(ann);
  return 0;
}
