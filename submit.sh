#!/bin/bash

[[ $# -eq 1 ]] || { echo FAILED; exit 1; }

curl \
  --form file=@"predictions$1.txt" \
  --form username="mmm" \
  --form key="05382278" \
  --form submit="Submit" \
  "http://www.mimuw.edu.pl/~janusza/" \
  | grep -q "successful upload" \
  && { echo OK; } || { echo FAILED; exit 1; }

